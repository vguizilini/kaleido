#include <jni.h>
#include <string>

extern "C"
{

JNIEXPORT jstring JNICALL
Java_com_example_vguizilini_kaleidocpp_MainActivity_keyFromJNI(
        JNIEnv *env, jobject)
{
    std::string key1 = "140"    , key2 = "B41"   , key3 = "B22A2" , key4 = "9D";
    std::string key5 = "EB4061" , key6 = "BDA6F" , key7 = "b6"    , key8 = "747E1" , key9 = "4";
    std::string key = key1 + key2 + key3 + key4 + key5 + key6 + key7 + key8 + key9;
    return env->NewStringUTF( key.c_str() );
}

JNIEXPORT jstring JNICALL
Java_com_example_vguizilini_kaleidocpp_MainActivity_ivFromJNI(
        JNIEnv *env, jobject)
{
    std::string iv1 = "0D" , iv2 = "7" , iv3 = "9A874" , iv4 = "BE09" , iv5 = "C72F";
    std::string iv = iv1 + iv2 + iv3 + iv4 + iv5;
    return env->NewStringUTF( iv.c_str() );
}

}