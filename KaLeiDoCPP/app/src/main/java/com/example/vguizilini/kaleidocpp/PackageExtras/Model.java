package com.example.vguizilini.kaleidocpp.PackageExtras;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import com.example.vguizilini.kaleidocpp.PackageFilter.Filter;
import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.io.FileInputStream;

public class Model
{
    private int[] pixels;
    private long[] input_size;
    private float[] inps, outs;
    private int w, h, wh, whc;
    private String model_file, input_node, output_node;
    private Bitmap icon;

    private String key, iv;

    private TensorFlowInferenceInterface inferenceInterface;
    private String dir_models = Constants.dir_models().toString();

    private boolean has_changed;

////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// CONSTRUCTOR

    public Model( Context context , int icon , String key, String iv )
    {
        this.key = key; this.iv = iv;
        this.icon = BitmapFactory.decodeResource( context.getResources() , icon ).copy(
                Bitmap.Config.ARGB_8888 , true );
        has_changed = true;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// ASYNCTASK

    public Bitmap doInBackground( Bitmap input )
    {
        try
        {
            if( has_changed )
            {
                FileInputStream file = new FileInputStream(dir_models + model_file );
                inferenceInterface =  new TensorFlowInferenceInterface( Encryptor.decode( key , iv , file ) );
                has_changed = false;
            }
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }

        int orig_w = input.getWidth();
        int orig_h = input.getHeight();

        input = resizeBitmap( input , w , h );
        input.getPixels( pixels , 0 , w , 0 , 0 , w , h );
        pixel2input();

        inferenceInterface.feed( input_node , inps , input_size );
        inferenceInterface.run( new String[]{ output_node } );
        inferenceInterface.fetch( output_node , outs );

        input2pixel();
        Bitmap output = Bitmap.createBitmap( pixels , w , h , input.getConfig() );
        output = resizeBitmap( output , orig_w , orig_h );
        output = addIconToOutput( output );

        return output;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// FUNCTIONS

    public void update( Filter filter )
    {
        model_file = filter.getModelFile();
        input_node = filter.getInputNode();
        output_node = filter.getOutputNode();
        input_size = filter.getInputSize();

        w = (int)input_size[0];
        h = (int)input_size[1];

        wh = w * h;
        whc = wh * (int)input_size[2];

        pixels = new int[ wh  ];
        inps = new float[ whc ];
        outs = new float[ whc ];

        has_changed = true;
    }

    private static Bitmap resizeBitmap( Bitmap bitmap , int width , int height )
    {
        return Bitmap.createScaledBitmap( bitmap , width , height , false );
    }

    private void pixel2input()
    {
        for( int i = 0 ; i < wh ; i++ )
        {
            inps[ 3 * i     ] = Color.red(   pixels[ i ] );
            inps[ 3 * i + 1 ] = Color.green( pixels[ i ] );
            inps[ 3 * i + 2 ] = Color.blue(  pixels[ i ] );
        }
    }

    private void input2pixel()
    {
        for( int i = 0 ; i < wh ; i++ )
        {
            int r = Math.min( 255 , Math.max( 0 , (int)outs[ 3 * i     ] ) );
            int g = Math.min( 255 , Math.max( 0 , (int)outs[ 3 * i + 1 ] ) );
            int b = Math.min( 255 , Math.max( 0 , (int)outs[ 3 * i + 2 ] ) );
            pixels[i] = Color.rgb( r , g , b );
        }
    }

    private Bitmap addIconToOutput( Bitmap output )
    {
        int ow = output.getWidth() , oh = output.getHeight();
        int dims = Math.min( ow , oh ) / 8;

        Bitmap resicon = resizeBitmap( icon , dims , dims );

        int iw = resicon.getWidth(); ow = ow - iw;
        int ih = resicon.getHeight(); oh = oh - ih;

        for( int w = 0 ; w < iw ; w++ )
        {
            for (int h = 0; h < ih ; h++ )
            {
                int a = Color.alpha( resicon.getPixel( w , h ) );
                if( a > 0 ) output.setPixel(ow + w, oh + h , resicon.getPixel( w , h) );
            }
        }

        resicon.recycle();
        return output;
    }
}
