package com.example.vguizilini.kaleidocpp.PackageExtras;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

public class Saver
{
    private Context context;
    private Activity activity;

    public File dir_images = Constants.dir_images();
    public File dir_models = Constants.dir_models();
    public String url_path   = Constants.url_path();

    public Saver( Context context , Activity activity )
    {
        this.context = context;
        this.activity = activity;

        getStoragePermission();

    }

    public boolean checkStoragePermission()
    {
        int checkRead    = ContextCompat.checkSelfPermission( this.context , Manifest.permission.READ_EXTERNAL_STORAGE  );
        int checkWrite   = ContextCompat.checkSelfPermission( this.context , Manifest.permission.WRITE_EXTERNAL_STORAGE );
        int checkNetwork = ContextCompat.checkSelfPermission( this.context , Manifest.permission.ACCESS_NETWORK_STATE   );
        return (checkRead == 0) & (checkWrite == 0) & (checkNetwork == 0);
    }

    private void getStoragePermission()
    {
        if( !checkStoragePermission() )
        {
            ActivityCompat.requestPermissions( this.activity , new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE  } , 0 );
            ActivityCompat.requestPermissions( this.activity , new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE } , 0 );
            ActivityCompat.requestPermissions( this.activity , new String[]{ Manifest.permission.ACCESS_NETWORK_STATE   } , 0 );
        }
    }

    public void scanNewFile( File file )
    {
        MediaScannerConnection.scanFile( this.context , new String[]{ file.toString() } , null ,
                new MediaScannerConnection.OnScanCompletedListener()
                {
                    public void onScanCompleted( String path , Uri uri )
                    {
                        Log.i("ExternalStorage" , "Scanned " + path + ":" );
                        Log.i("ExternalStorage" , "-> uri="  + uri        );
                    }
                });
    }

    public String image( Bitmap bitmap )
    {
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String name = "Image-" + n + ".jpg";
        File file = new File( dir_images , name );

        try
        {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress( Bitmap.CompressFormat.JPEG , 100 , out );
            out.flush(); out.close();
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }

        scanNewFile( file );
        return file.toString();
    }

    public boolean url(String model_file , ListenerDownloadLoop listener )
    {
        try
        {
            InputStream input;
            FileOutputStream output;
            HttpURLConnection connection;

            URL url = new URL( url_path + model_file );
            connection = (HttpURLConnection)url.openConnection();
            connection.setConnectTimeout( 5000 );
            connection.setReadTimeout( 5000 );
            connection.connect();

            if( connection.getResponseCode() != HttpURLConnection.HTTP_OK )
                return false;

            File model_path = dir_models;
            String[] model_dirs = model_file.split("/");
            for( int i = 0 ; i < model_dirs.length - 1 ; i++ )
            {
                model_path = new File( model_path , model_dirs[i] );
                model_path.mkdirs();
            }

            File file = new File( dir_models , model_file );

            input = connection.getInputStream();
            output = new FileOutputStream( file );

            boolean success = listener.runDownloadLoop( input , output , connection );

            input.close(); output.close();
            connection.disconnect();

            if( success ) scanNewFile( file );
            return success;
        }
        catch( Exception e )
        {
            e.printStackTrace();
            return false;
        }

    }

    public static interface ListenerDownloadLoop
    {
        boolean runDownloadLoop(InputStream input, FileOutputStream output, HttpURLConnection connection);
    }
}
