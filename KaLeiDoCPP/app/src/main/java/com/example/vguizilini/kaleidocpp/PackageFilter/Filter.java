package com.example.vguizilini.kaleidocpp.PackageFilter;

public class Filter
{
    private int image, price, progress;
    private long[] input_size = { 0 , 0 , 0 };
    private String name, model_file, input_node, output_node;

    public Filter( String name , int image ,
                   String model_file , String input_node , String output_node , long[] input_size ,
                   int price )
    {
        this.setName( name );
        this.setImage( image );
        this.setModelFile( model_file );
        this.setInputNode( input_node );
        this.setOutputNode( output_node );
        this.setInputSize( input_size );
        this.setPrice( price );
        this.setProgress( 0 );
    }

    public String getName() { return this.name; }
    public void setName( String name ) { this.name = name; }

    public int getImage() { return this.image; }
    public void setImage( int image ) { this.image = image; }

    public String getModelFile() { return this.model_file; }
    public void setModelFile( String model_file ) { this.model_file = "/" + model_file; }

    public String getInputNode() { return this.input_node; }
    public void setInputNode( String input_node ) { this.input_node = input_node; }

    public String getOutputNode() { return this.output_node; }
    public void setOutputNode( String output_node ) { this.output_node = output_node; }

    public long[] getInputSize() { return this.input_size; }
    public void setInputSize( long[] input_size ) { this.input_size = input_size; }

    public int getPrice() { return this.price; }
    public String getPriceString() { return Integer.toString( this.price ); }
    public void setPrice( int price ) { this.price = price; }

    public int getProgress() { return this.progress; }
    public void setProgress( int progress ) { this.progress = progress; }

    public boolean available()   { return this.progress == 0; }
    public boolean downloading() { return this.progress > 0 && this.progress < 100; }
    public boolean purchased()   { return this.progress == 100; }

    public boolean equal( Filter filter ) { return this.name.equals( filter.name ); }

}
