package com.example.vguizilini.kaleidocpp.PackageExtras;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Tickets
{
    private static final String num_tickets = "num_tickets";
    private SharedPreferences sharedPreferences;

    public int get()
    {
        return sharedPreferences.getInt( num_tickets , 0 );
    }
    public String getString() { return Integer.toString( get() ); }

    public Tickets( Context context )
    {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences( context );
        if( !sharedPreferences.contains( num_tickets ) ) update( 20 );
    }

    public void update( int n )
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt( num_tickets , n ).apply();
    }

    public void add( int n )
    {
        update( get() + n );
    }
    public void sub( int n ) { update( get() - n ); }
}
