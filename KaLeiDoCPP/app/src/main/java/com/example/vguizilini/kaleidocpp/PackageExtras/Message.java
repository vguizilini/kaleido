package com.example.vguizilini.kaleidocpp.PackageExtras;

import android.widget.Toast;
import android.content.Context;

public class Message
{
    public static void show( Context context , String message )
    {
        Toast toast = Toast.makeText( context , message , Toast.LENGTH_SHORT );
        toast.show();

    }
}
