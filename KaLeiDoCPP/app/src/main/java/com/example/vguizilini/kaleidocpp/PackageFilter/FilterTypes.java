package com.example.vguizilini.kaleidocpp.PackageFilter;

import java.util.LinkedList;
import com.example.vguizilini.kaleidocpp.PackageExtras.Constants;

public class FilterTypes
{
    private LinkedList<LinkedList<Filters>> all_filters;

    public FilterTypes(Filters.ListenerFilterClick listener )
    {
        all_filters = Constants.filterTypes( listener );
    }

    public Filters get( int i , int j )
    {
        return all_filters.get( i ).get( j );
    }
}
