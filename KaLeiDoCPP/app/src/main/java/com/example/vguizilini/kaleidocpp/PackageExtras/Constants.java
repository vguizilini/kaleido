package com.example.vguizilini.kaleidocpp.PackageExtras;

import android.os.Environment;

import com.example.vguizilini.kaleidocpp.PackageFilter.Filters;

import java.io.File;
import java.util.LinkedList;

public class Constants
{
    private static String root_images()
    {
        return Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DCIM ).toString();
    }

    private static String root_models()
    {
        return Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS ).toString();
    }

    static File dir_images()
    {
        File dir_images = new File( root_images() , "/KaLeiDo/" );
        dir_images.mkdirs();
        return dir_images;
    }

    static File dir_models()
    {
        File dir_models = new File( root_models() , "/KaLeiDo/" );
        dir_models.mkdirs();
        return dir_models;
    }

    static String url_path()
    {
        return "https://phyllopod-core.000webhostapp.com/";
    }

    static public String[] levelStrings( int level )
    {
        String[] data = null;

        switch( level )
        {
            case -1:
                data = new String[] {
                        "Stylize",
                        "Enhance",
                }; break;
            case 0:
                data = new String[] {
                        "Images",
                        "Painters",
                }; break;
            case 1:
                data = new String[] {
                        "Super Resolution",
                        "Colorization",
                }; break;
        }

        return data;
    }

    static public LinkedList<LinkedList<Filters>> filterTypes( Filters.ListenerFilterClick listener )
    {
        LinkedList<LinkedList<Filters>> filterTypes = new LinkedList<>();

        LinkedList<Filters> filters0 = new LinkedList<>();
        filters0.add( new Filters( listener , "type1" ) );
        filters0.add( new Filters( listener , "type2" ) );
        filterTypes.add( filters0 );

        LinkedList<Filters> filters1 = new LinkedList<>();
        filters1.add( new Filters( listener , "type1" ) );
        filters1.add( new Filters( listener , "type2" ) );
        filterTypes.add( filters1 );

        return filterTypes;
    }


}
