package com.example.vguizilini.kaleidocpp.PackageFilter;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Environment;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vguizilini.kaleidocpp.R;

import java.io.File;
import java.util.List;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.MyViewHolder>
{
    private int selection = 0;//RecyclerView.NO_POSITION;

    private List<Filter> filterList;
    private Filters.ListenerFilterClick filterClickListener;

    private String MODEL_PREFIX = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS ).toString()
            + "/KaLeiDo/";

    public int getSelection() { return selection; }

    public FilterAdapter( List<Filter> filterList , Filters.ListenerFilterClick filterClickListener )
    {
        this.filterList = filterList;
        this.filterClickListener = filterClickListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener , View.OnLongClickListener
    {
        public TextView name;
        public ImageView image;
        public ConstraintLayout block;

        public MyViewHolder( View view )
        {
            super(view);

            name = view.findViewById( R.id.name );
            image = view.findViewById( R.id.imageFilter);
            block = view.findViewById( R.id.block );

            view.setTag( view );
            view.setOnClickListener( this );
            view.setOnLongClickListener( this );
        }

        @Override
        public void onClick( View view )
        {
            filterClickListener.onClick( view , getAdapterPosition() );
            notifyItemChanged( selection );
            selection = getLayoutPosition();
            notifyItemChanged( selection );
        }

        @Override
        public boolean onLongClick( View view )
        {
            filterClickListener.onLongClick( view , getAdapterPosition() );
            notifyItemChanged( selection );
            selection = getLayoutPosition();
            notifyItemChanged( selection );
            return true;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder( ViewGroup parent , int viewType )
    {
        View view = LayoutInflater.from( parent.getContext() )
                .inflate( R.layout.block_filter, parent , false );

        return new MyViewHolder( view );
    }

    @Override
    public void onBindViewHolder( MyViewHolder holder , int position )
    {
        Filter filter = filterList.get( position );

        holder.name.setText( filter.getName() );
        holder.image.setImageResource( filter.getImage() );

        if( selection == position )
        {
            holder.name.setTypeface(null, Typeface.BOLD_ITALIC);
            holder.name.setTextSize( TypedValue.COMPLEX_UNIT_SP , 16f );
            holder.block.setBackgroundResource( R.drawable.filter_border_selected);
        }
        else
        {
            holder.name.setTypeface(null, Typeface.NORMAL);
            holder.name.setTextSize( TypedValue.COMPLEX_UNIT_SP , 14f );
            holder.block.setBackgroundResource( R.drawable.filter_border_not_selected );
        }

        if( checkPurchase( filter ) )
            holder.name.setTextColor( Color.BLACK );
        else holder.name.setTextColor( Color.LTGRAY  );
    }

    @Override
    public int getItemCount()
    {
        return filterList.size();
    }

    public boolean checkPurchase( Filter filter )
    {
        try
        {
            File file = new File ( MODEL_PREFIX , filter.getModelFile() );
            return file.exists();
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }

        return false;
    }
}