package com.example.vguizilini.kaleidocpp.PackageFilter;

import android.view.View;

import com.example.vguizilini.kaleidocpp.R;

import java.util.List;
import java.util.ArrayList;

public class Filters
{
    private FilterAdapter filterAdapter;
    private List<Filter> filterList = new ArrayList<>();

    public FilterAdapter getAdapter() { return filterAdapter; }
    public Filter get( int position ) { return filterList.get( position ); }

    public Filters( ListenerFilterClick listener , String type )
    {
        filterAdapter = new FilterAdapter( filterList , listener );

        String input_node1 = "input" , output_node1 = "clip_by_value";
        long[] input_size1 = { 256 , 256 , 3 }, input_size2 = { 512 , 512 , 3 }, input_size3 = { 1024 , 1024 , 3 };
        int price1 = 15;

        switch( type )
        {
            case "type1":

                filterList.add(new Filter("Rain Princess", R.drawable.styles_rain_princess,
                        "fast_style_transfer/rain_princess_frozen.cry",
                        input_node1, output_node1, input_size1, price1));

                filterList.add(new Filter("Colors256F", R.drawable.styles_alpha_colors,
                        "fast_style_transfer/alpha_colors_256_frozen.cry",
                        input_node1, output_node1, input_size1, price1));

                filterList.add(new Filter("Colors256Q", R.drawable.styles_alpha_colors,
                        "fast_style_transfer/alpha_colors_256_quantized.cry",
                        input_node1, output_node1, input_size1, price1));

                filterList.add(new Filter("Colors512F", R.drawable.styles_alpha_colors,
                        "fast_style_transfer/alpha_colors_512_frozen.cry",
                        input_node1, output_node1, input_size2, price1));

                filterList.add(new Filter("Colors512Q", R.drawable.styles_alpha_colors,
                        "fast_style_transfer/alpha_colors_512_quantized.cry",
                        input_node1, output_node1, input_size2, price1));

                filterList.add(new Filter("Colors1024F", R.drawable.styles_alpha_colors,
                        "fast_style_transfer/alpha_colors_1024_frozen.cry",
                        input_node1, output_node1, input_size3, price1));

                filterList.add(new Filter("Colors1024Q", R.drawable.styles_alpha_colors,
                        "fast_style_transfer/alpha_colors_1024_quantized.cry",
                        input_node1, output_node1, input_size3, price1));


                filterList.add(new Filter("Artistic_512", R.drawable.styles_artistic,
                        "fast_style_transfer/artistic_512_frozen.cry",
                        input_node1, output_node1, input_size2, price1));

                filterList.add(new Filter("Black Swirls_256", R.drawable.styles_black_swirls,
                        "fast_style_transfer/black_swirls_256_frozen.cry",
                        input_node1, output_node1, input_size1, price1));

                filterList.add(new Filter("Black Swirls_512", R.drawable.styles_black_swirls,
                        "fast_style_transfer/black_swirls_512_frozen.cry",
                        input_node1, output_node1, input_size2, price1));

                break;

            case "type2":

                filterList.add(new Filter("BlehBlehBleh", R.drawable.styles_rain_princess,
                        "fast_style_transfer/rain_princess_frozen.pb",
                        input_node1, output_node1, input_size1, price1));

                filterList.add(new Filter("BlahBlahBlah", R.drawable.styles_rain_princess,
                        "fast_style_transfer/rain_princess_frozen.cry",
                        input_node1, output_node1, input_size1, price1));
                break;
        }

        filterAdapter.notifyDataSetChanged();
    }

    public static interface ListenerFilterClick
    {
        void onClick(View view, int position);
        void onLongClick(View view, int position);
    }
}
