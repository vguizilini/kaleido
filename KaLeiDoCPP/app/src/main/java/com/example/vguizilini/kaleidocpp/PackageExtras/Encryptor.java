package com.example.vguizilini.kaleidocpp.PackageExtras;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Encryptor
{
    public static InputStream decode( String key_string , String iv_string , FileInputStream finput )
    {
        try
        {
//            SecretKey key = new SecretKeySpec( key_string.getBytes( "UTF-8" ) , "AES" );
//            IvParameterSpec iv = new IvParameterSpec( iv_string.getBytes( "UTF-8") );
//            Cipher cipher = Cipher.getInstance( "AES/CFB8/NoPadding" );
//            cipher.init( Cipher.DECRYPT_MODE , key , iv );

            SecretKey key = new SecretKeySpec( key_string.getBytes( "UTF-8" ) , "ARC4" );
            Cipher cipher = Cipher.getInstance( "ARC4" );
            cipher.init( Cipher.DECRYPT_MODE , key );

            CipherInputStream cinput = new CipherInputStream( finput , cipher );
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            byte[] b = new byte[1024]; int bytesRead;
            while( ( bytesRead = cinput.read(b) ) >= 0 )
                output.write( b , 0 , bytesRead );

            return new ByteArrayInputStream( output.toByteArray() );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
            return null;
        }
    }

    public static String decode( String key_string , String iv_string , String file )
    {
        try
        {
            SecretKey key = new SecretKeySpec( key_string.getBytes( "UTF-8" ) , "AES" );
            IvParameterSpec iv = new IvParameterSpec( iv_string.getBytes( "UTF-8") );

            String input = Constants.dir_models() + "/" + file;

            Cipher cipher = Cipher.getInstance( "AES/CFB8/NoPadding" );
            cipher.init( Cipher.DECRYPT_MODE , key , iv );

            FileInputStream finput = new FileInputStream( input );
            CipherInputStream cinput = new CipherInputStream( finput , cipher );
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            byte[] b = new byte[1024]; int bytesRead;
            while( ( bytesRead = cinput.read(b) ) >= 0 )
                output.write( b , 0 , bytesRead );

            return new String( output.toByteArray() );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
            return "FAILURE";
        }
    }
}

