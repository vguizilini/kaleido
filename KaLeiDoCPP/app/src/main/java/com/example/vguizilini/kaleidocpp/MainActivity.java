package com.example.vguizilini.kaleidocpp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.example.vguizilini.kaleidocpp.PackageExtras.Functions;
import com.example.vguizilini.kaleidocpp.PackageExtras.Model;
import com.example.vguizilini.kaleidocpp.PackageExtras.Saver;
import com.example.vguizilini.kaleidocpp.PackageExtras.Tickets;
import com.example.vguizilini.kaleidocpp.PackageExtras.Message;

import com.example.vguizilini.kaleidocpp.PackageFilter.Filter;
import com.example.vguizilini.kaleidocpp.PackageFilter.Filters;
import com.example.vguizilini.kaleidocpp.PackageFilter.FilterTypes;

import com.example.vguizilini.kaleidocpp.PackageDropDown.DropDowns;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity implements Filters.ListenerFilterClick
{
    static { System.loadLibrary("native-lib"); }
    public native String keyFromJNI();
    public native String ivFromJNI();

    Model model;
    Saver saver;
    Tickets tickets;

    Filters filters;
    FilterTypes filterTypes;

    DropDowns dropdowns;
    int level1, level2;

    Uri image;
    LinkedList<Bitmap> bitmaps;
    int curr_bitmap;

    private static final int MAX_IMAGE_SIZE = 512;
    private static final int RESULT_LOAD_IMAGE = 1;
    Filter curr_filter;

    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;

    Toolbar toolbar;
    AsyncTask asyncDownload;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        level1 = level2 = 0;
        filterTypes = new FilterTypes( this );
        bitmaps = new LinkedList<>();

        startSaver();
        startModel();
        startFilters();
        startTickets();
        startToolbar();
        startGestures();
        startDropDowns();

        updateProcess();
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// BEHAVIORS

    private class GestureListener extends GestureDetector.SimpleOnGestureListener
    {
        @Override
        public boolean onFling( MotionEvent e1 , MotionEvent e2 , float velocityX , float velocityY )
        {
            if( e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs( velocityX ) > SWIPE_THRESHOLD_VELOCITY )
            {   // MOVE LAST
                if( curr_bitmap > 0 )
                {
                    curr_bitmap -= 1;
                    updateImageProcess();
                }
                return true;
            }
            else if( e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs( velocityX ) > SWIPE_THRESHOLD_VELOCITY )
            {   // MOVE NEXT
                if( curr_bitmap < bitmaps.size() - 1 )
                {
                    curr_bitmap += 1;
                    updateImageProcess();
                }
                return true;
            }

            if( e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE && Math.abs( velocityY ) > SWIPE_THRESHOLD_VELOCITY)
            {   // SHARE
                if( bitmaps.size() > 0 )
                {
                    Intent share = new Intent( Intent.ACTION_SEND );
                    share.setType( "image/jpeg" );
                    share.putExtra( Intent.EXTRA_STREAM, Functions.bmp2uri( getBaseContext() , bitmaps.getLast() ) );
                    startActivity( Intent.createChooser( share , "KaLeiDo Sharing" ) );
                }
                return true;
            }
            else if( e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY )
            {   // SAVE
                if( bitmaps.size() > 0 )
                {
                    String path = saver.image( bitmaps.getLast() );
                    String message = saver.checkStoragePermission() ? "SAVED: " + path : "COULD NOT SAVE";
                    Message.show( getApplicationContext() , message );
                }
                return true;
            }
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        getMenuInflater().inflate( R.menu.main_menu , menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        switch( item.getItemId() )
        {
            case R.id.delete:
                Message.show( this , "DELETE" );
                return true;
            default:
                return super.onOptionsItemSelected( item );
        }
    }

    @Override
    public void onClick(View view , int position )
    {
        curr_filter = filters.get( position );
        updateSelection();

        RelativeLayout detail = findViewById( R.id.viewDetail  );
        if( detail.getVisibility() == View.VISIBLE ) updateDetails();
    }

    @Override
    public void onLongClick( View view , int position )
    {
        curr_filter = filters.get( position );

        RelativeLayout detail  = findViewById( R.id.viewDetail  );
        RelativeLayout process = findViewById( R.id.viewProcess );

        TransitionManager.beginDelayedTransition( detail );
        TransitionManager.beginDelayedTransition( process );

        int visible = detail.getVisibility();

        if( visible == View.VISIBLE )
        {
            process.setVisibility( View.VISIBLE   );
            detail.setVisibility(  View.INVISIBLE );
        }
        else
        {
            updateDetails();
            process.setVisibility( View.INVISIBLE );
            detail.setVisibility(  View.VISIBLE   );
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// LOAD IMAGE

    @Override
    protected void onActivityResult( int requestCode , int resultCode , Intent data )
    {
        super.onActivityResult( requestCode , resultCode , data );

        if( requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null )
        {
            try
            {
                image = data.getData();

                for( int i = 0 ; i < bitmaps.size() ; i++ )
                    bitmaps.get( i ).recycle();
                bitmaps.clear(); curr_bitmap = 0;

                Bitmap temp1 = Functions.uri2bmp( this.getBaseContext() , image , MAX_IMAGE_SIZE );
                bitmaps.add( temp1.copy( Bitmap.Config.RGB_565 , true ) );
                temp1.recycle();

                updateImageProcess();
                updateSelection();
            }
            catch( Exception e )
            {
                e.printStackTrace();
            }
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// RUN MODEL

    private class runModel extends AsyncTask< Void , Integer , Bitmap >
    {
        @Override
        protected void onPreExecute()
        {
            updateEnabled( false );
        }

        @Override
        protected Bitmap doInBackground( Void ... voids )
        {
            return model.doInBackground( bitmaps.get( curr_bitmap ) );
        }

        @Override
        protected void onPostExecute( Bitmap bitmap )
        {
            while( bitmaps.size() > curr_bitmap + 1 )
            {
                bitmaps.get( curr_bitmap + 1 ).recycle();
                bitmaps.remove( curr_bitmap + 1 );
            }

            bitmaps.add( bitmap );
            curr_bitmap += 1;

            updateImageProcess();
            updateEnabled( true );
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// RUN DOWNLOAD

    private class runDownload extends AsyncTask< Filter , Integer , Boolean >
    {
        ProgressBar bar = findViewById( R.id.progressDownload );
        Filter down_filter;

        @Override
        protected Boolean doInBackground( Filter... filters )
        {
            return saver.url(filters[0].getModelFile(), new Saver.ListenerDownloadLoop()
            {
                @Override
                public boolean runDownloadLoop( InputStream input , FileOutputStream output ,
                                                HttpURLConnection connection )
                {
                    try
                    {
                        byte data[] = new byte[4096]; long total = 0; int count;
                        int fileLength = connection.getContentLength();

                        while( ( count = input.read(data) ) != -1 )
                        {
                            total += count;
                            if (fileLength > 0)
                                publishProgress( (int)( total * 100 / fileLength ) );
                            output.write(data, 0, count);
                            if( isCancelled() ) return false;
                        }
                    }
                    catch( Exception e ) { return false; }
                    return true;
                }
            });
        }

        @Override
        protected void onPreExecute()
        {
            down_filter = curr_filter;
            bar.setProgress( 1 );

            down_filter.setProgress( 1 );
            updateTicketCounter( - down_filter.getPrice() );
            updateDetails();
        }

        @Override
        protected void onProgressUpdate( Integer... progress )
        {
            down_filter.setProgress( progress[0] );
            if( curr_filter.equal( down_filter ) )
                bar.setProgress( progress[0] );
        }

        @Override
        protected void onCancelled( Boolean result )
        {
            down_filter.setProgress( 0 );
            updateTicketCounter( + down_filter.getPrice() );
            Message.show( getBaseContext() , "CANCELLING" );
            updateDetails();
        }

        @Override
        protected void onPostExecute( Boolean result )
        {
            if( !result )
            {
                down_filter.setProgress( 0 );
                updateTicketCounter( + down_filter.getPrice() );
                Message.show( getBaseContext() , "DOWNLOAD ERROR" );
            }

            updateDetails();
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// FUNCTION BUTTONS

    ////// BUTTON LOAD
    public void funcButtonLoad( View view )
    {
        Intent i = new Intent( Intent.ACTION_PICK ,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI );
        startActivityForResult( i , RESULT_LOAD_IMAGE );
    }

    ////// BUTTON CONVERT
    public void funcButtonConvert( View view )
    {
        new runModel().execute();
    }

    ////// BUTTON BUY
    public void funcButtonBuy( View view )
    {
        if( curr_filter.available() )
            asyncDownload = new runDownload().execute( curr_filter );
        else if( curr_filter.downloading() )
            asyncDownload.cancel(true);
    }

    ////// BUTTON SAVE
    public void funcButtonSave( View view )
    {
        String path = saver.image( bitmaps.getLast() );
        String message = saver.checkStoragePermission() ? "SAVED: " + path : "COULD NOT SAVE";
        Message.show( getApplicationContext() , message );
    }

    ////// BUTTON SHARE
    public void funcButtonShare( View view )
    {
        Intent share = new Intent( Intent.ACTION_SEND );
        share.setType( "image/jpeg" );
        share.putExtra( Intent.EXTRA_STREAM , Functions.bmp2uri( this , bitmaps.getLast() ) );
        startActivity( Intent.createChooser( share , "KaLeiDo Sharing" ) );
    }

    ////// BUTTON TICKETS
    public void funcButtonTickets( View view )
    {
        updateTicketCounter( 10 );
    }

    ////// BUTTON LAST
    public void funcButtonLast( View view )
    {
        if( curr_bitmap > 0 )
        {
            curr_bitmap -= 1;
            updateImageProcess();
        }
    }

    ////// BUTTON NEXT
    public void funcButtonNext( View view )
    {
        if( curr_bitmap < bitmaps.size() - 1 )
        {
            curr_bitmap += 1;
            updateImageProcess();
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// STARTS

    /////// START SAVER
    private void startSaver()
    {
        saver = new Saver( this , this );
    }

    /////// START MODEL
    private void startModel()
    {
        model = new Model( this , R.drawable.icon_kaleido , keyFromJNI() , ivFromJNI() );
    }

    /////// START TICKETS
    private void startTickets()
    {
        tickets = new Tickets( this );
        updateTicketCounter();
    }

    /////// START FILTERS
    private void startFilters()
    {
        RecyclerView.LayoutManager layoutManager =
                new LinearLayoutManager( getApplicationContext() ,
                        LinearLayoutManager.HORIZONTAL , false );

        RecyclerView filterView = findViewById( R.id.recyclerFilter );
        filterView.setLayoutManager( layoutManager );
        filterView.setItemAnimator( new DefaultItemAnimator() );
        filterView.getItemAnimator().setChangeDuration(0);
    }

    /////// START DROPDOWNS
    private  void startDropDowns()
    {
        dropdowns = new DropDowns( this , this ,
                new DropDowns.ListenerLevelChange()
                {
                    @Override
                    public void change( int position )
                    {
                        level1 = position;
                    }
                } ,
                new DropDowns.ListenerLevelChange()
                {
                    @Override
                    public void change( int position )
                    {
                        level2 = position;
                        filters = filterTypes.get( level1 , level2 );
                        curr_filter = filters.get( filters.getAdapter().getSelection() );

                        final RecyclerView filterView = findViewById(R.id.recyclerFilter);
                        filterView.setAdapter( filters.getAdapter() );
                        updateSelection();
                    }
                });
    }

    /////// START GESTURES
    private void startGestures()
    {
        final GestureDetector gesture = new GestureDetector( this , new GestureListener() );
        final ImageView imageProcess = findViewById( R.id.imageProcess );
        imageProcess.setOnTouchListener( new View.OnTouchListener()
        {
            @Override
            public boolean onTouch( final View view , final MotionEvent event )
            {
                gesture.onTouchEvent( event ); return true;
            }
        });
    }

    /////// START TOOLBARS
    private void startToolbar()
    {
        toolbar = findViewById( R.id.toolbarMenu );
        setSupportActionBar( toolbar );

        ActionBar action = getSupportActionBar();
        action.setDisplayHomeAsUpEnabled( false );
        action.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM );
        action.setCustomView( R.layout.block_toolbar);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// UPDATES

    /////// UPDATE SELECTION
    private void updateSelection()
    {
        model.update( curr_filter );
        ImageButton buttonConvert = findViewById( R.id.buttonConvert );
        buttonConvert.setVisibility( ( filters.getAdapter().checkPurchase( curr_filter ) && bitmaps.size() > 0 ) ?
                View.VISIBLE : View.INVISIBLE );
    }

    /////// UPDATE DETAILS
    private void updateDetails()
    {
        ImageView image = findViewById( R.id.imageDetail );
        image.setImageResource( curr_filter.getImage() );

        boolean can_buy = tickets.get() > curr_filter.getPrice();

        TextView price = findViewById(R.id.textPrice);
        price.setText( curr_filter.getPriceString() );
        price.setTextColor( can_buy ? Color.GREEN : Color.RED );
        price.setVisibility( curr_filter.available() ? View.VISIBLE : View.INVISIBLE );

        ImageButton icon = findViewById( R.id.buttonBuy );
        icon.setImageResource( curr_filter.available() ? R.drawable.icon_buy :
                curr_filter.downloading() ? R.drawable.icon_stop : R.drawable.icon_check );
        icon.setEnabled( curr_filter.downloading() || can_buy );

        ProgressBar bar = findViewById( R.id.progressDownload );
        bar.setVisibility( curr_filter.downloading() ? View.VISIBLE : View.INVISIBLE );
        if( curr_filter.downloading() ) bar.setProgress( curr_filter.getProgress() );
    }

    /////// UPDATE PROCESS
    private void updateProcess()
    {
        ImageButton buttonLast = findViewById( R.id.buttonLast );
        ImageButton buttonNext = findViewById( R.id.buttonNext );
        ImageButton buttonConvert = findViewById( R.id.buttonConvert );

        buttonLast.setVisibility( curr_bitmap > 0                  ? View.VISIBLE : View.INVISIBLE );
        buttonNext.setVisibility( curr_bitmap < bitmaps.size() - 1 ? View.VISIBLE : View.INVISIBLE );
        buttonConvert.setVisibility( bitmaps.size() > 0            ? View.VISIBLE : View.INVISIBLE );
    }

    /////// UPDATE IMAGE PROCESS
    private void updateImageProcess()
    {
        ImageView imageProcess = findViewById( R.id.imageProcess );
        imageProcess.setImageDrawable(new BitmapDrawable( getBaseContext().getResources() , bitmaps.get( curr_bitmap ) ) );
        updateProcess();
    }

    /////// UPDATE TICKET COUNTER
    private void updateTicketCounter()
    {
        TextView textTickets1 = findViewById( R.id.textTickets1 );
        textTickets1.setText( tickets.getString() );

        TextView textTickets2 = findViewById( R.id.textTickets2 );
        textTickets2.setText( tickets.getString() );
    }

    /////// UPDATE TICKET COUNTER
    private void updateTicketCounter( int n )
    {
        tickets.add( n );
        updateTicketCounter();
        updateDetails();
    }

    /////// UPDATE ENABLED
    private void updateEnabled( boolean flag )
    {
        ProgressBar progress = findViewById( R.id.progressModel );
        progress.setVisibility( flag ? View.INVISIBLE : View.VISIBLE );

        ImageButton buttonLoad     = findViewById( R.id.buttonLoad     );
        ImageButton buttonConvert  = findViewById( R.id.buttonConvert  );
        ImageButton buttonSave     = findViewById( R.id.buttonSave     );

        buttonLoad.setEnabled(     flag );
        buttonConvert.setEnabled(  flag );
        buttonSave.setEnabled(     flag );
    }


////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// INTERFACE FUNCTIONS

}













