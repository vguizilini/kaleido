package com.example.vguizilini.kaleidocpp.PackageExtras;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.example.vguizilini.kaleidocpp.R;

import java.io.ByteArrayOutputStream;

public class Functions
{
    static public Bitmap uri2bmp(Context context , Uri uri , final int max_image_size )
    {
        try
        {
            BitmapFactory.Options options1 = new BitmapFactory.Options(); options1.inJustDecodeBounds = true;
            BitmapFactory.decodeStream( context.getContentResolver().openInputStream( uri ) , null , options1 );

            int scale = 0;
            int max = Math.max( options1.outWidth , options1.outHeight );

            do { scale += 1;
            } while( max / scale > max_image_size );

            BitmapFactory.Options options2 = new BitmapFactory.Options(); options2.inSampleSize = scale;

            Bitmap raw = BitmapFactory.decodeStream( context.getContentResolver().openInputStream(uri) , null , options2 );
            Bitmap bmp = raw.copy( Bitmap.Config.RGB_565 , true );
            raw.recycle(); return bmp;

        }
        catch( Exception e )
        {
            e.printStackTrace();
            return null;
        }
    }

    static public Uri bmp2uri( Context context , Bitmap bmp )
    {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress( Bitmap.CompressFormat.JPEG , 100 , bytes );
        String path = MediaStore.Images.Media.insertImage( context.getContentResolver() , bmp , "Title" , null );
        return Uri.parse( path );
    }
}
