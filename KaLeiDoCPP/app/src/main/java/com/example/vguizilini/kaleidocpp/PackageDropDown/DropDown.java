package com.example.vguizilini.kaleidocpp.PackageDropDown;

import android.app.Activity;
import android.content.Context;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.vguizilini.kaleidocpp.PackageExtras.Constants;
import com.example.vguizilini.kaleidocpp.R;

public class DropDown
{
    private Spinner spinner;
    private Activity activity;
    private Context context;

    public DropDown( Activity activity , Context context , int level , AdapterView.OnItemSelectedListener listener )
    {
        this.activity = activity;
        this.context = context;
        changeLevel( level );
        spinner.setOnItemSelectedListener( listener );
    }

    public void changeLevel( int level)
    {
        spinner = activity.findViewById( level == -1 ? R.id.spinnerLevel0 : R.id.spinnerLevel1 );
        ArrayAdapter<String> adapter = new ArrayAdapter<>( context,
                android.R.layout.simple_spinner_item , Constants.levelStrings( level ) );
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        spinner.setAdapter( adapter );
    }

    public int getNumberItems()
    {
        return spinner.getAdapter().getCount();
    }

    public int getSelection()
    {
        return spinner.getSelectedItemPosition();
    }

    public void setSelection( int position )
    {
        spinner.setSelection( position );
    }

}
