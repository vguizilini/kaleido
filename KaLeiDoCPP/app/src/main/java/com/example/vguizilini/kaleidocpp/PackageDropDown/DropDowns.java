package com.example.vguizilini.kaleidocpp.PackageDropDown;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;

public class DropDowns
{
    private DropDown dropdown1, dropdown2;

    private int[] levels;
    private int prev_position = 0;

    public DropDowns( Activity activity , Context context ,
                      final ListenerLevelChange changeLevel1 ,
                      final ListenerLevelChange changeLevel2 )
    {
        dropdown1 = new DropDown( activity , context , -1 , new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent , View view , int position , long id )
            {
                levels[ prev_position ] = dropdown2.getSelection();
                prev_position = position;

                changeLevel1.change( position );
                dropdown2.changeLevel( position );
                dropdown2.setSelection( levels[ position ] );
            }

            @Override
            public void onNothingSelected( AdapterView<?> parent ) {} });

        dropdown2 = new DropDown( activity , context ,  0 , new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected( AdapterView<?> parent , View view , int position , long id )
            {
                changeLevel2.change( position );
            }

            @Override
            public void onNothingSelected( AdapterView<?> parent ) {} });

        int num_items = dropdown1.getNumberItems();
        levels = new int[ num_items ];
        for( int i = 0 ; i < num_items ; i++ )
            levels[i] = 0;
    }

    public static interface ListenerLevelChange
    {
        void change(int position);
    }
}
